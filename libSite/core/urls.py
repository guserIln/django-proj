"""
URL configuration for libSite project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from core.views import AddBook, HomePage, Book, BookList, BookDistribution

urlpatterns = [
    path('', HomePage.as_view(), name="HomePage"),
    path('books/', BookList.as_view(), name="books"),
    path('distr_books/', BookDistribution.as_view(), name="distr_books"),
    path('book/<id>/', Book.as_view(), name="book"),
    path('books/add/', AddBook.as_view(), name="add_book")
]
