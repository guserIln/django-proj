from datetime import datetime

from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import ListView, DetailView

from . import models, forms
from .models import book_distribution


# Create your views here.
class HomePage(View):
    def get(self, request):
        context = {'text': 'Главная страница', 'title': 'Главная'}
        return render(request=request, template_name="index.html", context=context)


class BookList(ListView):
    template_name = 'books.html'
    extra_content = {'title': 'Студенты'}
    context_object_name = 'books'
    model = models.book_list


class Book(DetailView):
    template_name = 'book.html'
    model = models.Book
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        book = self.object
        title = book.title
        return super().get_context_data(name=title)


class BookDistribution(View):
    def get(self, request):
        book_distr = book_distribution.objects.all()
        context = {'text': 'Главная страница', 'title': 'Книги', 'book_distr': book_distr}
        return render(request=request, template_name="book_distribution.html", context=context)


class AddBook(View):
    def get(self, request):
        form = forms.Book()
        return render(request, template_name='add_book.html', context={'title': 'Новая книга', 'form': form})

    def post(self, request):
        form = forms.Book(request.POST)
        if form.is_valid():
            title = form.cleaned_data.get('title')
            artikul = form.cleaned_data.get('artikul')
            publisher = form.cleaned_data.get('publisher')
            book = models.Book(title=title)
            if artikul:
                book.artikul = artikul
            if publisher:
                book.publisher = publisher
            book.release_date = datetime.now().date()
            book.save()
            return redirect('books')
        else:
            return self.get(self, request)
