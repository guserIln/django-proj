from django.contrib import admin

from core.models import Book
from core.models import Visitor
from core.models import book_list
from core.models import Sotrudnik
from core.models import book_distribution
from core.models import book_distribution_table
from core.models import book_return
from core.models import Genre
# Register your models here.
admin.site.register(Book)
admin.site.register(Visitor)
admin.site.register(Genre)
admin.site.register(book_list)
admin.site.register(Sotrudnik)
admin.site.register(book_distribution)
admin.site.register(book_distribution_table)
admin.site.register(book_return)
