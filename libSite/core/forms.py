from django import forms


class Book(forms.Form):
    title = forms.CharField(label='Название', max_length=255, required=True)
    author = forms.CharField(label='Автор', max_length=100, required=True)
    artikul = forms.CharField(label='Артикул', max_length=11, required=True)
    publisher = forms.CharField(max_length=255)
