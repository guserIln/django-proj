from django.db import models


class Genre(models.Model):
    name = models.CharField(max_length=200, help_text="Enter genre")

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=100)
    artikul = models.CharField(name='artikul', max_length=11)
    release_date = models.DateField(name='release_date', default=True)
    genre = models.ManyToManyField(Genre, help_text="Select a genre")
    publisher = models.CharField(name="publisher", max_length=255)

    def __str__(self):
        return self.title


class Visitor(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class book_list(models.Model):
    book_name = models.ForeignKey(to='Book', on_delete=models.PROTECT)
    count = models.IntegerField()

    def __str__(self):
        return self.book_name.title


class Sotrudnik(models.Model):
    name = models.CharField(max_length=255)


class book_distribution(models.Model):
    date = models.DateField(),
    visitor_name = models.ForeignKey(to='Visitor', on_delete=models.PROTECT)
    sotrudnik = models.ForeignKey(to='Sotrudnik', on_delete=models.PROTECT)


class book_distribution_table(models.Model):
    book_distribution = models.ForeignKey(to='book_distribution', on_delete=models.PROTECT)
    book = models.ForeignKey(to='Book', on_delete=models.PROTECT),
    count = models.IntegerField()


class book_return(models.Model):
    date = models.DateField(),
    visitor_name = models.ForeignKey(to='Visitor', on_delete=models.PROTECT)
    sotrudnik = models.ForeignKey(to='Sotrudnik', on_delete=models.PROTECT)


class book_return_table(models.Model):
    book_return = models.ForeignKey(to='book_return', on_delete=models.PROTECT)
    book = models.ForeignKey(to='Book', on_delete=models.PROTECT),
    count = models.IntegerField()
